<?php namespace ErrorBuilder;

use InvalidArgumentException;

/**
 * Class ErrorCodes
 *
 * When adding a new exception please remember to add it to the array of details too
 *
 * @package Lib
 * @see https://confluence.aminocom.com/display/BPLAT/Error+codes
 */
class ErrorCodes
{
    const UNHANDLED_EXCEPTION = '65A5PG';

    const ACCESS_FORBIDDEN = 'A95TLW';

    const BAD_REQUEST = 'H2KR5S';

    const RESOURCE_NOT_FOUND = 'B53S5U';

    const METHOD_NOT_ALLOWED = '4INZEX';

    const NOT_AUTHENTICATED = '9YR6ZI';

    const HTTP_FORBIDDEN = '6G6WOS';

    const OAUTH_EXCEPTION = 'NMUVPD';

    private $errorDetails = [

        self::UNHANDLED_EXCEPTION => [
            'short_description' => 'Unhandled exception',
            'http_status_code'  => 500,
        ],

        self::BAD_REQUEST => [
            'short_description' => 'Bad request',
            'http_status_code'  => 400,
        ],

        self::NOT_AUTHENTICATED => [
            'short_description' => 'Authentication required',
            'http_status_code'  => 401,
        ],

        self::ACCESS_FORBIDDEN => [
            'short_description' => 'Access forbidden',
            'http_status_code'  => 403,
        ],

        self::RESOURCE_NOT_FOUND => [
            'short_description' => 'Resource not found',
            'http_status_code'  => 404,
        ],

        self::METHOD_NOT_ALLOWED => [
            'short_description' => 'Method not allowed',
            'http_status_code'  => 405,
        ],

        self::OAUTH_EXCEPTION => [
            'short_description' => 'OAuth exception',
            'http_status_code' => 400,
        ],

        self::HTTP_FORBIDDEN => [
            'short_description' => 'HTTP forbidden',
            'http_status_code' => 403,
        ]

    ];

    /**
     * @param string $errorCode
     * @return array
     * @throws InvalidArgumentException
     */
    public function getErrorCodeDetails(string $errorCode)
    {        
        if (isset($this->errorDetails[$errorCode])) {

            return $this->errorDetails[$errorCode];

        }

        throw new InvalidArgumentException("Could not find code [$errorCode]");
    }
}