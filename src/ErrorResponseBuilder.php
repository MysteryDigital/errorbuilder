<?php namespace ErrorBuilder;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseBuilder
 * @package App\Http\ResponseHelper
 *
 * This class builds JSON API 1.0 structured responses.
 * It also helps to build standard error responses.
 */
class ErrorResponseBuilder
{
    const JSON_API_CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * Builds an error response that can be returned to the client.
     *
     * @param ResponseInterface $response
     * @param string $errorCode
     * @param string $details
     * @param string|null $title
     * @param int|null $httpCode
     * @return ResponseInterface
     */
    public function buildError(
        ResponseInterface $response,
        string $errorCode,
        string $details,
        string $title = null,
        integer $httpCode = null
    ) {

        if (is_null($title)) {

            $errorCodes = new ErrorCodes();

            $errorDetails = $errorCodes->getErrorCodeDetails($errorCode);

            $title = $title ?? $errorDetails['short_description'];

        }

        if (is_null($httpCode)) {

            $errorCodes = new ErrorCodes();

            $errorDetails = $errorCodes->getErrorCodeDetails($errorCode);

            $httpCode = $httpCode ?? $errorDetails['http_status_code'];
        }

        $bodyArray = $this->buildBodyArray($errorCode, $details, $title);

        $body = $response->getBody();

        $body->write(json_encode($bodyArray));

        // response is immutable so you need to declare a new instance
        $response = $response->withStatus($httpCode, $title)
                             ->withHeader('Content-Type', self::JSON_API_CONTENT_TYPE)
                             ->withBody($body);

        return $response;
    }

    /**
     * @param string $errorCode
     * @param string $details
     * @param string|null $title
     * @return array
     */
    private function buildBodyArray(string $errorCode, string $details, string $title = null)
    {
        if (is_null($title)) {

            $errorCodes = new ErrorCodes();

            $errorDetails = $errorCodes->getErrorCodeDetails($errorCode);

            $title = $title ?? $errorDetails['short_description'];

        }


        $errorEntity = [
            'id'      => bin2hex(random_bytes(8)),
            'href'    => null,
            'code'    => $errorCode,
            'title'   => $title,
            'details' => $details,
        ];

        // build the error entity
        $errorResponseArray = [
            "errors" => [
                $errorEntity,
            ],
        ];

        return $errorResponseArray;

    }

}