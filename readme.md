[![CodeClimate](http://img.shields.io/codeclimate/github/andybeak/errorbuilder.svg?style=flat)](https://codeclimate.com/github/andybeak/errorbuilder "CodeClimate")
[![CircleCI](https://circleci.com/gh/andybeak/errorbuilder.svg?style=shield)](https://circleci.com/gh/andybeak/errorbuilder)
# Error builder

A simple helper to allow creating PSR7 compliant responses that contain a JSON API 1.0 compliant error.

## Usage

```
$response = (new ErrorResponseBuilder())->buildError($response, ErrorCodes::HTTP_FORBIDDEN, 'You did not provide authentication information');
```

## Running tests

You can run the tests with `./vendor/bin/phpunit`
