<?php namespace Tests\Functional;

use ErrorBuilder\{
    ErrorResponseBuilder, ErrorCodes
};
use \ReflectionClass;
use GuzzleHttp\Psr7\Response;

use Psr\Http\Message\ResponseInterface;

class ErrorResponseBuilderTest extends \PHPUnit\Framework\TestCase
{
    const JSON_API_CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * @expectedException InvalidArgumentException
     */
    public function testGettingWrongCode()
    {
        $errorCodes = new ErrorCodes();

        $errorCodes->getErrorCodeDetails('THIS CODE DOES NOT EXIST AND WILL NEVER EXIST');

    }

    public function testGettingErrorCodeDetails()
    {
        $errorCodes = new ErrorCodes();

        $reflection = new \ReflectionClass(get_class($errorCodes));

        $errorDetails = \PHPUnit\Framework\Assert::readAttribute($errorCodes, "errorDetails");

        $codes = $reflection->getConstants();

        foreach ($codes as $constant => $code ) {

            $returnedDetails = $errorCodes->getErrorCodeDetails($code);

            $this->assertSame($returnedDetails, $errorDetails[$code]);

        }
    }

    public function testHttpErrorIsCorrect()
    {
        $errorBuilder = new ErrorResponseBuilder();

        // using a real implementation is a lot easier than mocking, because we rely on the result of getBody too
        $response = new Response;

        $errorCodes = new ErrorCodes();

        $codes = (new ReflectionClass(ErrorCodes::class))->getConstants();

        foreach ($codes as $tag => $code) {

            $errorDetails = $errorCodes->getErrorCodeDetails($code);

            $randomDetails = bin2hex(random_bytes(20));

            $randomTitle = bin2hex(random_bytes(4));

            $response = $errorBuilder->buildError($response, $code, $randomDetails);

            $this->assertSame($response->getStatusCode(), $errorDetails['http_status_code']);

            $contentHeader = $response->getHeader('Content-Type');

            $this->assertSame(reset($contentHeader), self::JSON_API_CONTENT_TYPE);

        }
    }

    public function testOmittingOptionalParametersFillsInFromDefault()
    {
        $errorBuilder = new ErrorResponseBuilder();

        // using a real implementation is a lot easier than mocking, because we rely on the result of getBody too
        $response = new Response;

        $reflection = new \ReflectionClass(get_class($errorBuilder));

        $method = $reflection->getMethod('buildBodyArray');

        $method->setAccessible(true);

        $codes = (new ReflectionClass(ErrorCodes::class))->getConstants();

        foreach ($codes as $tag => $code) {

            $httpBody = $method->invokeArgs($errorBuilder, [$code, 'Detailed message']);

            $this->assertArrayHasKey('errors', $httpBody);

            $errors = $httpBody['errors'][0];

            $requiredKeys = ['id', 'href', 'code', 'title', 'details'];

            foreach ($requiredKeys as $requiredKey) {
                $this->assertArrayHasKey($requiredKey, $errors);
            }

            $this->assertSame($errors['code'], $code);

            $this->assertSame($errors['details'], 'Detailed message');

            $this->assertNull($errors['href']);

            $this->assertNotEmpty($errors['title']);

        }

    }

    public function testOverwriteTitleAndHttpCode()
    {
        $errorBuilder = new ErrorResponseBuilder();

        // using a real implementation is a lot easier than mocking, because we rely on the result of getBody too
        $response = new Response;

        $reflection = new \ReflectionClass(get_class($errorBuilder));

        $method = $reflection->getMethod('buildBodyArray');

        $method->setAccessible(true);

        $codes = (new ReflectionClass(ErrorCodes::class))->getConstants();

        foreach ($codes as $tag => $code) {

            $randomTitle = bin2hex(random_bytes(4));

            $randomHttpStatus = rand(200, 599);

            $httpBody = $method->invokeArgs($errorBuilder, [$code, 'Detailed message', $randomTitle, $randomHttpStatus]);

            $this->assertArrayHasKey('errors', $httpBody);

            $errors = $httpBody['errors'][0];

            $requiredKeys = ['id', 'href', 'code', 'title', 'details'];

            foreach ($requiredKeys as $requiredKey) {
                $this->assertArrayHasKey($requiredKey, $errors);
            }

            $this->assertSame($errors['code'], $code);

            $this->assertSame($errors['details'], 'Detailed message');

            $this->assertNull($errors['href']);

            $this->assertSame($errors['title'], $randomTitle);

        }
    }
}